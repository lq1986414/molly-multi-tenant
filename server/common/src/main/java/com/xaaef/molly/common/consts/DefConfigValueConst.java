package com.xaaef.molly.common.consts;

/**
 * <p>
 * 全局配置文件 key
 * </p>
 *
 * @author Wang Chen Chen
 * @version 1.0.1
 * @date 2021/7/16 15:15
 */

public class DefConfigValueConst {

    /**
     * 每个租户的 默认项目ID
     */
    public static final Long DEFAULT_PROJECT_ID = 10001L;


    /**
     * 每个租户的 默认部门ID
     */
    public static final Long DEFAULT_DEPT_ID = 10001L;

    /**
     * 每个租户的 默认角色 ID
     */
    public static final Long DEFAULT_ROLE_ID = 10001L;


}
